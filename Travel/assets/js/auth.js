const resetInput = $("#resetInput"),
  passwordInput = $("#password"),
  emailInput = $("#email-address");

// for login form
$("form#loginForm").submit(function (e) {
  e.preventDefault();
  if (emailInput.val() != "" && passwordInput.val() != "") {
    Util.login(emailInput.val(), passwordInput.val(), (res) => {
      console.log(res);
    });
  } else {
    Util.showError("Please enter email and password", "Login");
  }
});

// $("form#resetForm").submit(function (e) {
//   e.preventDefault();
//   if (resetInput.val() != "") {
//     Util.forgotPassword(resetInput.val(), (res) => {
//       console.log(res);
//     });
//   } else {
//     Util.showError("Please enter email and password", "Login");
//   }
//   $.ajax({
//     url: "some-url",
//     type: "post",
//     dataType: "json",
//     data: $("form#resetForm").serialize(),
//     success: function (data) {
//       // ... do something with the data...
//     },
//   });
// });
