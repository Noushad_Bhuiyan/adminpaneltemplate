var serverInfo = {
  baseUrl: "https://holiday-hype.herokuapp.com/",
  //  baseUrl: "https://run.mocky.io",
  // prefix: "/v3",
  prefix: "api/v1",
  auth: "/users",
};
var emailServerInfo = {
  //baseUrl:'',
  prefix: "/api/v1",
  auth: "/auth",
};

var pages = {
  index: "auth.html",
  admin: "index.html",
  notAdmin: "https://holiday-hype.herokuapp.com/",
};
var localDataKey = {
  authInfo: "auth_info",
  userData: "user_data",
};
var UserData = {
  token: "",
  _id: "",
  photo: "",
  name: "",
  email: "",
  role: "",
  setData: function (d) {
    UserData.token = d.token;
    UserData.photo = d.photo;
    UserData._id = d._id;
    UserData.name = d.name;
    UserData.email = d.email;
    UserData.role = d.role;
    if (!Util.isEmpty(d.token)) {
      Util.setLocalData(localDataKey.authInfo, UserData);
    }
  },
  resetData: function () {
    Util.removeLocalData(localDataKey.authInfo);
    UserData.setData({
      token: "",
      photo: "",
      _id: "",
      name: "",
      email: "",
      role: "",
    });
  },
  refreshData: function () {
    d = Util.getLocalData(localDataKey.authInfo);
    UserData.setData(d);
  },
};
var workingScope = {
  faculty: { id: null, name: null },
  program: { id: null, name: null },
  year: { id: null, name: null },
  setData: function (d) {
    workingScope.faculty = d.faculty;
    workingScope.program = d.program;
    workingScope.year = d.year;
    Util.setLocalData(localDataKey.workingScope, workingScope);
  },
  refreshData: function () {
    d = Util.getLocalData(localDataKey.workingScope);
    if (d != null) {
      workingScope.setData(d);
    }
  },
};
var Validator = {
  GetSkippedIds: function (containerId) {
    var dlg;
    if (Util.isEmpty(containerId)) {
      dlg = $("body");
    } else {
      dlg = $(`#${containerId}`);
    }
    return $(dlg).find(`:input[skip-validation]`);
  },
  ValidateIn: function (continerId = "", skipIds = "") {
    var dlg;
    if (Util.isEmpty(continerId)) {
      dlg = $("body");
    } else {
      dlg = $(`#${continerId}`);
    }
    var skippableInputs = skipIds.split(",");
    var skippableInputs2 = Validator.GetSkippedIds(continerId);
    var inputs = $(dlg).find(`:input[required]`);
    for (var k = 0; k < inputs.length; k++) {
      var el = inputs[k];

      if (skippableInputs.filter((x) => x == el.id).length > 0) {
        continue;
      }

      if (skippableInputs2.filter((x) => x == el.id).length > 0) {
        continue;
      }

      if (el.validationMessage != "") {
        var msg = el.validity.patternMismatch ? el.title : el.validationMessage;
        var title = "unknown";
        if (el.labels.length > 0) {
          title = el.labels[0].textContent;
        } else if (el.placeHolder) {
          title = el.placeHolder;
        } else if (!Util.isEmpty(el.id)) {
          title = el.id;
        }
        Util.showError(msg, title);
        el.focus();
        return false;
      }
    }
    return true;
  },
};
var FileUploader = {
  uploadFiles: function (fileNames) {
    $.each(filename, (i, v) => {
      var data = new FormData();
      data.append(v, file);

      //Creating an XMLHttpRequest and sending

      Util.setUploadingFile();
      var xhr = new XMLHttpRequest();
      xhr.open(
        "POST",
        serverInfo.baseUrl + serverInfo.prefix + "/upload-file/" + code
      );
      xhr.send(data);
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
          Util.resetUploadingFile();
          callback(JSON.parse(xhr.response));
        }
        return false;
      };
    });
  },
};
var Util = {
  isValidImageFileToUpload: function (ctrl, extraTypes = [], maxLimitInMB = 2) {
    var file = ctrl[0].files[0];
    if (file === undefined || file == null) {
      return true;
    }
    var fileType = file["type"];
    var validImageTypes = ["image/gif", "image/jpeg", "image/jpg", "image/png"];
    for (var k = 0; k < extraTypes.length; k++) {
      validImageTypes.push(extraTypes[k]);
    }
    return (
      $.inArray(fileType, validImageTypes) >= 0 &&
      file.size / 1024 / 1024 <= maxLimitInMB
    );
  },
  clearCache: function () {
    localStorage.clear();
    Util.showInfo("Please login again");
    setTimeout(() => {
      window.location.href = "index.html";
    }, 500);
  },
  //do projection
  project: function (obj, projObj) {
    let projectedObj = {};
    for (let key in projObj) {
      projectedObj[key] = obj[key];
    }
    return projectedObj;
  },
  deepClone: function (data) {
    return JSON.parse(JSON.stringify(data));
  },
  defaultErrCallback: function (err) {
    var txt = err.statusText;

    if (txt.length > 19 && txt.substr(0, 19) == "Validation Failed. ") {
      var obj = JSON.parse(txt.substr(19));
      toastr.error(obj[Object.keys(obj)[0]], Object.keys(obj)[0]);
      console.log(err);
      return;
    }
    if (txt != undefined) {
      toastr.error(err.responseJSON.message, "Error: " + err.statusText);
      if (err.status == 401) {
        Util.clearCache();
        Util.applyAuthGuard();
      }
      console.log(err);
      return;
    }
    if (err.responseText === undefined) {
      toastr.error("Server is not accessible", "Connection");
      console.log(err);
      return;
    }

    toastr.error(txt.responseText, "Error: " + txt.statusText);
    console.log(err);
  },
  initDates: function (scopeId = "") {
    var scope = scopeId == "" ? "" : `#${scopeId} `;
    $(`${scopeId}input[type="date"]`).each(function (i, v) {
      document.getElementById(v.id).valueAsDate = new Date();
    });
  },
  getDate: function (days = 0) {
    var d = new Date();
    d = d.addDays(days);
    return d.toISOString().split("T")[0];
  },
  busy: false,
  ongoingSaveOrDelete: false, //to check multiple save of single object in slow internet
  setOngingSaveOrDelete: function () {
    Util.ongoingSaveOrDelete = true;
  },
  resetOngingSaveOrDelete: function () {
    Util.ongoingSaveOrDelete = false;
  },
  uploadingFile: false, //to check multiple save of single object in slow internet
  setUploadingFile: function () {
    Util.uploadingFile = true;
  },
  resetUploadingFile: function () {
    Util.uploadingFile = false;
  },

  overlayDivId: "overlay",

  switchOverlay: function (show) {
    Util.busy = show;
    var overlay = $("#" + Util.overlayDivId);
    $(overlay).css("display", show ? "" : "none");
  },

  getDdlText(ddlId) {
    return $("#" + ddlId + " option:selected").text();
  },

  setDdlText(ddlId, txt, trigger = true) {
    $(`#${ddlId} option`)
      .filter(function () {
        return $.trim($(this).text()) == $.trim(txt);
      })
      .prop("selected", true);
    if (trigger) {
      $("#" + ddlId).change();
    }
  },

  getDefaultActionButtons: function () {
    var editBtn =
      '<button type="button" class="btn btn-info btn-sm save mr-2 tk-edit">Edit</button>';
    var removeBtn =
      '<button type="button" class="btn btn-danger btn-sm remove tk-delete">Delete</button>';
    return editBtn + removeBtn;
  },

  resolveActiveText: function (isActive) {
    return isActive ? "Yes" : "No";
  },

  prepareFileUrl: function (fileId) {
    return serverInfo.baseUrl + serverInfo.prefix + "/download-file/" + fileId;
  },

  login: function (
    email,
    pass,
    callback,
    errorCallback = Util.defaultErrCallback
  ) {
    var payload = { email: email, password: pass };
    console.log(payload);

    $.ajax({
      type: "POST",
      url: serverInfo.baseUrl + serverInfo.prefix + serverInfo.auth + "/login",
      data: JSON.stringify(payload),
      contentType: "application/json",
      success: function (res) {
        console.log("login success", res);
        UserData.setData({
          token: res.token,
          _id: res.data.user._id,
          photo: res.data.user.photo,
          name: res.data.user.name,
          email: res.data.user.email,
          role: res.data.user.role,
        });

        if (callback != null) {
          callback(res);
        }
        res.data.user.role == "admin"
          ? Util.showSuccess(res.status, "Login")
          : Util.showInfo(
              "Only admin is allowed. You are not authorized to access this page",
              "Redirecting to home page"
            );
        setTimeout(() => {
          Util.applyAuthGuard();
        }, 3000);
      },

      error: function (res) {
        if (errorCallback == null) {
          console.log("error occured in login", res);
          toastr.error(res.status, "Error");
        } else {
          errorCallback(res);
        }
      },
    });
  },

  changePassword: function (
    email,
    oldPass,
    newPass,
    callback,
    errorCallback = Util.defaultErrCallback
  ) {
    UserData.refreshData();
    var payload = { email: email, oldPassword: oldPass, newPassword: newPass };
    $.ajax({
      type: "POST",
      url: serverInfo.baseUrl + serverInfo.auth + "/changepassword",
      data: JSON.stringify(payload),
      contentType: "application/json",
      beforeSend: function (req) {
        if (UserData.token.length > 0) {
          req.setRequestHeader("Authorization", "Bearer " + UserData.token);
        }
        Util.switchOverlay(true);
      },
      error: function (res) {
        if (errorCallback == null) {
          console.log("error occured in login", res);
          toastr.error(res.statusText, "Error");
        } else {
          errorCallback(res);
        }
      },
      success: function (res) {
        if (callback != null) {
          callback(res);
        }
      },
    });
  },

  logout: function (callback, errorCallback = Util.defaultErrCallback) {
    $.ajax({
      type: "GET",
      url: serverInfo.baseUrl + serverInfo.prefix + serverInfo.auth + "/logout",
      contentType: "application/json",
      beforeSend: function (req) {
        UserData.refreshData();
        if (UserData.token.length > 0) {
          req.setRequestHeader("Authorization", "Bearer " + UserData.token);
        }
        Util.switchOverlay(true);
      },
      error: function (res) {
        //Util.removeLocalData(localDataKey.authInfo);
        if (errorCallback == null) {
          console.log("error occured in forget password request", res);
          toastr.error(res.statusText, "Error");
        } else {
          errorCallback(res);
        }
      },
      success: function (res) {
        if (callback != null) {
          callback(res);
        }
        Util.removeLocalData(localDataKey.authInfo);
        Util.applyAuthGuard();
      },
    });
  },
  isLoggedIn: function () {
    var d = Util.getLocalData(localDataKey.authInfo);
    return !Util.isEmpty(d) && !Util.isEmpty(d.token);
  },

  getCurrentFileName: function () {
    return window.location.pathname.split("/").reverse()[0];
  },

  applyAuthGuard: function () {
    //unauthorized
    if (!Util.isLoggedIn() && Util.getCurrentFileName() != pages.index) {
      window.location.href = pages.index;
    }

    //not admin
    if (!Util.isAdmin() && Util.isLoggedIn()) {
      window.location.href = pages.notAdmin;
    }

    //admin
    if (Util.isAdmin() && Util.getCurrentFileName() == pages.index) {
      window.location.href = pages.admin;
    }
  },

  isAdmin() {
    if (!Util.isLoggedIn()) return false;
    UserData.refreshData();
    return UserData.role.toLowerCase() == "admin";
  },
  getData: function (url, callback, errorCallback = Util.defaultErrCallback) {
    return $.ajax({
      type: "GET",
      url: serverInfo.baseUrl + serverInfo.prefix + url,
      contentType: "application/json",
      beforeSend: function (req) {
        UserData.refreshData();
        if (UserData.token.length > 0) {
          req.setRequestHeader("Authorization", "Bearer " + UserData.token);
        }
        Util.switchOverlay(true);
      },
      error: function (res) {
        if (errorCallback == null) {
          toastr.error(res.statusText, "Error");
        } else {
          errorCallback(res);
        }
      },
      success: function (res) {
        if (callback != null) {
          callback(res);
        } else {
          toastr.success(res.statusText, "Get Data");
        }
      },
    }).always(function () {
      Util.switchOverlay(false);
    });
  },

  createData: function (
    url,
    payload,
    callback,
    errorCallback = Util.defaultErrCallback
  ) {
    if (Util.ongoingSaveOrDelete) {
      Util.showInfo("Please wait for previous request", "Save");
      return;
    }
    Util.setOngingSaveOrDelete();
    if (payload.hasOwnProperty("_id")) delete payload._id;
    console.log(payload);
    $.ajax({
      type: "POST",
      url: serverInfo.baseUrl + serverInfo.prefix + url,
      data: JSON.stringify(payload),
      contentType: "application/json",
      beforeSend: function (req) {
        UserData.refreshData();
        if (UserData.token.length > 0) {
          req.setRequestHeader("Authorization", "Bearer " + UserData.token);
        }
        Util.switchOverlay(true);
      },
      error: function (res) {
        if (errorCallback == null) {
          console.log("error occured in saveData" + url, res);
          toastr.error(res.statusText, "Error");
        } else {
          errorCallback(res);
        }
      },
      success: function (res) {
        if (callback != null) {
          callback(res);
        } else {
          toastr.success(res.statusText, "Create Data");
        }
      },
    }).always(function () {
      Util.resetOngingSaveOrDelete();
      Util.switchOverlay(false);
    });
  },

  updateData: function (
    url,
    payload,
    callback,
    errorCallback = Util.defaultErrCallback
  ) {
    if (Util.ongoingSaveOrDelete) {
      Util.showInfo("Please wait for previous request", "Save");
      return;
    }
    Util.setOngingSaveOrDelete();
    console.log(payload);
    $.ajax({
      type: "PATCH",
      url: serverInfo.baseUrl + serverInfo.prefix + url,
      data: JSON.stringify(payload),
      contentType: "application/json",
      beforeSend: function (req) {
        UserData.refreshData();
        if (UserData.token.length > 0) {
          req.setRequestHeader("Authorization", "Bearer " + UserData.token);
        }
        Util.switchOverlay(true);
      },
      error: function (res) {
        if (errorCallback == null) {
          console.log("error occured in saveData" + url, res);
          toastr.error(res.statusText, "Error");
        } else {
          errorCallback(res);
        }
      },
      success: function (res) {
        if (callback != null) {
          callback(res);
        } else {
          toastr.success(res.statusText, "Save Data");
        }
      },
    }).always(function () {
      Util.resetOngingSaveOrDelete();
      Util.switchOverlay(false);
    });
  },

  deleteData: function (
    url,
    callback,
    errorCallback = Util.defaultErrCallback
  ) {
    if (Util.ongoingSaveOrDelete) {
      Util.showInfo("Please wait for previous request", "Delete");
      return;
    }
    Util.setOngingSaveOrDelete();
    console.log(serverInfo.baseUrl + serverInfo.prefix + url);
    $.ajax({
      type: "DELETE",
      url: serverInfo.baseUrl + serverInfo.prefix + url,
      contentType: "application/json",
      beforeSend: function (req) {
        UserData.refreshData();
        if (UserData.token.length > 0) {
          req.setRequestHeader("Authorization", "Bearer " + UserData.token);
        }
        Util.switchOverlay(true);
      },
      error: function (res) {
        if (errorCallback == null) {
          console.log("error occured in deleteData" + url, res);
          toastr.error(res.statusText, "Error");
        } else {
          errorCallback(res);
        }
      },
      success: function (res) {
        if (callback != null) {
          callback(res);
        } else {
          toastr.success(res.statusText, "Delete Data");
        }
      },
    }).always(function () {
      Util.resetOngingSaveOrDelete();
      Util.switchOverlay(false);
    });
  },

  gmethod: function (
    payload,
    callback,
    errorCallback = Util.defaultErrCallback,
    useMultiActionGuard = false,
    server = serverInfo
  ) {
    if (useMultiActionGuard) {
      if (Util.ongoingSaveOrDelete) {
        Util.showInfo("Please wait for previous request", "Wait");
        return;
      }
      Util.setOngingSaveOrDelete();
    }
    var url = "/method/GenericMethod";
    $.ajax({
      type: "POST",
      url: server.baseUrl + server.prefix + url,
      data: JSON.stringify(payload),
      contentType: "application/json",
      beforeSend: function (req) {
        UserData.refreshData();
        if (UserData.token.length > 0) {
          req.setRequestHeader("Authorization", "Bearer " + UserData.token);
        }
        Util.switchOverlay(true);
      },
      error: function (res) {
        if (errorCallback == null) {
          console.log("error occured in saveData" + url, res);
          toastr.error(res.statusText, "Error");
        } else {
          errorCallback(res);
        }
      },
      success: function (res) {
        if (callback != null) {
          callback(res);
        } else {
          toastr.success(res.statusText, "Create Data");
        }
      },
    }).always(function () {
      if (useMultiActionGuard) {
        Util.resetOngingSaveOrDelete();
      }
      Util.switchOverlay(false);
    });
  },
  uploadFile: function (inputId, code, callback) {
    var file = $("#" + inputId).prop("files")[0];
    if (file == undefined) {
      Util.resetUploadingFile();
      callback(null);
      return false;
    }
    if (Util.uploadingFile) {
      Util.showInfo("Please wait for previous request", "Wait");
      return;
    }
    var data = new FormData();
    data.append(file.name, file);
    UserData.refreshData();
    //Creating an XMLHttpRequest and sending

    Util.setUploadingFile();
    var xhr = new XMLHttpRequest();
    xhr.open(
      "POST",
      serverInfo.baseUrl + serverInfo.prefix + "/upload-file/" + code
    );
    xhr.send(data);
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        Util.resetUploadingFile();
        callback(JSON.parse(xhr.response));
      }
      return false;
    };
  },

  getLookupDataDdl: function (
    ddl,
    module,
    parentId,
    parentCode,
    parentName,
    defaultSelectText = "",
    textField = "name",
    callback = null
  ) {
    var payload = {
      code: "GetDdlData",
      type: "lookupdata-ddl",
      module: module,
      parentId: parentId,
      parentCode: parentCode,
      parentName: parentName,
    };

    $.ajax({
      type: "POST",
      url: serverInfo.baseUrl + serverInfo.prefix + "/method/GenericMethod",
      data: JSON.stringify(payload),
      contentType: "application/json",
      beforeSend: function (req) {
        if (UserData.token.length > 0) {
          req.setRequestHeader("Authorization", "Bearer " + UserData.token);
        }
      },
      error: function (res) {
        toastr.error(res.statusText, "Error");
      },
      success: function (res) {
        if (defaultSelectText == "") {
          defaultSelectText = "Select " + module;
        }
        if (ddl != null) {
          Util.populateDdl(ddl, res, defaultSelectText, textField, "key");
        }
        if (callback != null) {
          callback(res);
        }
      },
    });
  },
  getEnumDataDdl: function (enumName, ddl, defaultSelectText, cb) {
    var payload = {
      code: "GetDdlData",
      type: "enum-ddl",
      name: enumName,
    };
    Util.gmethod(payload, (res) => {
      if (ddl != null) {
        Util.populateDdl(ddl, res, defaultSelectText, "value", "key");
      }
      cb(res);
    });
  },

  discardTime: function (d) {
    if (d != null && d != undefined && d.length >= 10) {
      return d.substr(0, 10);
    }
    return d;
  },
  pickTime: function (d) {
    if (d != null && d != undefined && d.length >= 10) {
      return d.substr(11, 5);
    }
    return d;
  },

  populateDdl: function (ddl, data, defaultSelectText, textField, keyField) {
    $("#" + ddl).empty();
    $("<option/>")
      .attr("value", "")
      .text(defaultSelectText)
      .appendTo("#" + ddl);

    $.each(data, function () {
      $("<option/>")
        .attr("value", Object.byString(this, keyField))
        .text(Object.byString(this, textField))
        .appendTo("#" + ddl);
    });
  },
  initialiseDatatable: function (
    containerId,
    dtId,
    dataSet,
    columns,
    pageLength = 10,
    hideSearch = false,
    rowClickHandler = null,
    orderConfig = [[0, "asc"]]
  ) {
    rowClickHandler = rowClickHandler == null ? showDialog : rowClickHandler;

    $("#" + containerId).empty();
    $(
      '<table id="' +
        dtId +
        '" class="my-2 table table-striped table-bordered hover" width="100%"></table>'
    ).appendTo("#" + containerId);
    var dt = $("#" + dtId).DataTable({
      data: dataSet,
      columns: columns,
      order: orderConfig,
      pageLength: pageLength,
      searching: !hideSearch,
      paging: !hideSearch,
      info: !hideSearch,
      fnDrawCallback: function (oSettings) {
        $("#" + containerId + "_previous").text("«");
        $("#" + containerId + "_next").text("»");
      },
    });

    dt.off("click.rowClick").on("click.rowClick", "tbody tr", function (event) {
      if (event.target.classList.contains("tk-edit"))
        rowClickHandler(dt.row(this).data(), "U");
      else if (event.target.classList.contains("tk-delete"))
        rowClickHandler(dt.row(this).data(), "D");
      else if (event.target.classList.contains("tk-action")) {
        var data = $(event.target).attr("data-tk-action");
        rowClickHandler(dt.row(this).data(), data);
      }
    });
    $("#" + containerId + "_filter").addClass("form-inline");
    $("#" + containerId + "_filter input").addClass("form-control");

    $("#" + containerId + "_length").addClass("form-inline");
    $("#" + containerId + "_length select").addClass("form-control");
    $("#" + dtId + "_wrapper")
      .children(":first")
      .addClass("col-md-12");

    return dt;
  },

  setLocalData: function (key, valueObj) {
    window.localStorage.setItem(key, JSON.stringify(valueObj));
  },
  removeLocalData: function (key) {
    window.localStorage.removeItem(key);
  },

  getLocalData: function (key) {
    var value = window.localStorage.getItem(key);
    return JSON.parse(value);
  },

  showSuccess: function (msg, title = "Success") {
    toastr.success(msg, title);
  },

  showInfo: function (msg, title = "Info") {
    toastr.info(msg, title);
  },

  showError: function (msg, title = "Error") {
    toastr.error(msg, title);
  },

  filterAndGetProperty: function (arr, key, val, op = "eq", pName = "name") {
    var d = Util.filter(arr, key, val, op);
    return d.length > 0 ? d[0][pName] : "";
  },

  filter: function (arr, key, val, op = "eq") {
    if (op == "eq") {
      var ret = [];
      for (i in arr) {
        if (arr[i][key] == val) {
          ret.push(arr[i]);
        }
      }
      return ret;
    }
    if (op == "lt") {
      return $.filter(function (el) {
        return arr[key] < val;
      });
    }
    if (op == "lt") {
      return $.filter(function (el) {
        return arr[key] < val;
      });
    }
    if (op == "lte") {
      return $.filter(function (el) {
        return arr[key] <= val;
      });
    }
    if (op == "gt") {
      return $.filter(function (el) {
        return arr[key] > val;
      });
    }
    if (op == "gte") {
      return $.filter(function (el) {
        return arr[key] >= val;
      });
    }
  },
  removeElements: function (arr, elements) {
    for (var k = 0; k < elements.length; k++) {
      arr = arr.filter((x) => x != elements[k]);
    }
    return arr;
  },
  extend: function (obj1, obj2) {
    $.extend(true, obj1, obj2);
  },
  addColumnToDataset: function (dataset, propName, value) {
    $.each(dataset, function (i, v) {
      v[propName] = value;
    });
  },
  isEmpty: function (value) {
    //https://stackoverflow.com/questions/1812245/what-is-the-best-way-to-test-for-an-empty-string-with-jquery-out-of-the-box
    return (
      (typeof value == "string" && !value.trim()) ||
      typeof value == "undefined" ||
      value === null
    );
  },
  getQueryStringValue: function (name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  },
  clearAllInputs(divId) {
    $(`#${divId}`)
      .find(":input")
      .each(function () {
        if (this.type == "submit" || this.type == "button") {
          //do nothing
        } else if (this.type == "checkbox" || this.type == "radio") {
          this.checked = false;
        } else if (this.type == "file") {
          var control = $(this);
          control.replaceWith((control = control.clone(true)));
        } else {
          $(this).val("");
        }
      });
  },
  convertObjTo2DArray: function (objArray, propsArray = []) {
    var array = typeof objArray != "object" ? JSON.parse(objArray) : objArray;
    var out = [];

    if (propsArray.length == 0) {
      for (var i = 0; i < array.length; i++) {
        var row = [];
        var obj = array[i];
        for (var x in obj) {
          row.push(obj[x]);
        }
        out.push(row);
      }
    } else {
      for (var i = 0; i < array.length; i++) {
        var row = [];
        var obj = array[i];
        for (var j = 0; j < propsArray.length; j++) {
          row.push(obj.hasOwnProperty(propsArray[j]) ? obj[propsArray[j]] : "");
        }
        out.push(row);
      }
    }
    return out;
  },
};

//csv exporter

var CSVExporter = {
  convertToCSV: function (objArray) {
    var array = typeof objArray != "object" ? JSON.parse(objArray) : objArray;
    var str = "";

    for (var i = 0; i < array.length; i++) {
      var line = "";
      for (var index in array[i]) {
        if (line != "") line += ",";

        line += array[i][index];
      }

      str += line + "\r\n";
    }

    return str;
  },
  exportCSVFile: function (headers, items, fileTitle) {
    if (!Util.isEmpty(headers)) {
      items.unshift(headers);
    }
    // Convert Object to JSON
    var jsonObject = JSON.stringify(items);

    var csv = CSVExporter.convertToCSV(jsonObject);

    var exportedFilenmae = fileTitle + ".csv" || "export.csv";

    var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
    if (navigator.msSaveBlob) {
      // IE 10+
      navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
      var link = document.createElement("a");
      if (link.download !== undefined) {
        // feature detection
        // Browsers that support HTML5 download attribute
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", exportedFilenmae);
        link.style.visibility = "hidden";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  },
};

//excel exporter

var ExcelExporter = {
  createWorkBook: function (title, subject, author, createDate) {
    var wb = XLSX.utils.book_new();
    wb.Props = {
      Title: title,
      Subject: subject,
      Author: author,
      CreatedDate: createDate,
    };
    return wb;
  },
  addSheets: function (wb, sheetName, titleRow, objArray, propsArray = []) {
    var data = Util.convertObjTo2DArray(objArray, propsArray);
    data.unshift(titleRow);
    wb.SheetNames.push(sheetName);
    var wsData = XLSX.utils.aoa_to_sheet(data);
    wb.Sheets[sheetName] = wsData;
    return wb;
  },
  exportExcelFile: function (wb, filename) {
    var wbout = XLSX.write(wb, { bookType: "xlsx", type: "binary" });
    saveAs(
      new Blob([ExcelExporter.s2ab(wbout)], {
        type: "application/octet-stream",
      }),
      `${filename}.xlsx`
    );
  },
  s2ab: function (s) {
    var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
    var view = new Uint8Array(buf); //create uint8array as viewer
    for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xff; //convert to octet
    return buf;
  },
};
//static-images-gallary

var StaticIG = {
  getImageCounts: function (code, cb) {
    Util.getLookupDataDdl(
      null,
      "ImageContents",
      0,
      null,
      null,
      "",
      "name",
      (data) => {
        var name = Util.filterAndGetProperty(data, "code", code);
        cb(name);
      }
    );
  },
};

//data caching
DataCacher = {
  LoadWorkingScopeDataSources: function (cb) {
    Util.getLookupDataDdl(
      null,
      "Faculty",
      0,
      null,
      null,
      "",
      "name",
      (faculties) => {
        Util.getLookupDataDdl(
          null,
          "Program",
          0,
          null,
          null,
          "",
          "name",
          (programs) => {
            Util.getData("/getall/coachingyear", (coachingYears) => {
              Util.getData(
                "/getmany/programexam?currentYear=true",
                (pexams) => {
                  Util.getLookupDataDdl(
                    null,
                    "Subject",
                    0,
                    null,
                    null,
                    "",
                    "name",
                    (psubjects) => {
                      var data = {
                        faculties: faculties,
                        programs: programs,
                        coachingyears: coachingYears,
                        programExams: pexams,
                        programSubjects: psubjects,
                      };
                      Util.setLocalData(
                        localDataKey.workingScopeDataSources,
                        data
                      );
                      cb(data);
                    }
                  );
                }
              );
            });
          }
        );
      }
    );
  },
  GetWorkingScopeDataSources: function (cb) {
    var data = Util.getLocalData(localDataKey.workingScopeDataSources);
    if (Util.isEmpty(data)) {
      DataCacher.LoadWorkingScopeDataSources(cb);
    } else {
      cb(data);
    }
  },
};

//prototypes

Object.byString = function (o, s) {
  s = s.replace(/\[(\w+)\]/g, ".$1"); // convert indexes to properties
  s = s.replace(/^\./, ""); // strip a leading dot
  var a = s.split(".");
  for (var i = 0, n = a.length; i < n; ++i) {
    var k = a[i];
    if (k in o) {
      o = o[k];
    } else {
      return;
    }
  }
  return o;
};
Date.prototype.addDays = function (days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

//fake-data

FakeData = {
  getCities: function () {
    return [
      { code: "NY", name: "New York" },
      { code: "CA", name: "California" },
      { code: "ID", name: "Idaho" },
      { code: "MI", name: "Miami" },
    ];
  },
  getPackages: function (service) {
    return [
      {
        key: "starter",
        title: "Delaware Expediting Services",
        options: [
          {
            optionTitle: "Delaware 1hr rush filling",
            description:
              "For the speediest business formation, BizFilings offers a Rush Filing Service option to gain state approval of your incorporation in 1 hour.",
            price: "199",
            included: true,
          },
          {
            optionTitle: "Delaware 1hr rush filling",
            description:
              "For the speediest business formation, BizFilings offers a Rush Filing Service option to gain state approval of your incorporation in 1 hour.",
            price: "199",
            included: true,
          },
          {
            optionTitle: "Delaware 1hr rush filling",
            description:
              "For the speediest business formation, BizFilings offers a Rush Filing Service option to gain state approval of your incorporation in 1 hour.",
            price: "199",
            included: true,
          },
        ],
        packagePrice: 400,
        adonPrice: 300,
        stateFee: 30,
        totalPrice: 730,
      },
      {
        key: "walker",
        title: "Delaware Expediting Services-2",
        options: [
          {
            optionTitle: "Delaware 1hr rush filling",
            description:
              "For the speediest business formation, BizFilings offers a Rush Filing Service option to gain state approval of your incorporation in 1 hour.",
            price: "199",
            included: true,
          },
          {
            optionTitle: "Delaware 1hr rush filling",
            description:
              "For the speediest business formation, BizFilings offers a Rush Filing Service option to gain state approval of your incorporation in 1 hour.",
            price: "199",
            included: true,
          },
          {
            optionTitle: "Delaware 1hr rush filling",
            description:
              "For the speediest business formation, BizFilings offers a Rush Filing Service option to gain state approval of your incorporation in 1 hour.",
            price: "199",
            included: true,
          },
        ],
        packagePrice: 400,
        adonPrice: 300,
        stateFee: 30,
        totalPrice: 730,
      },
      {
        key: "runner",
        title: "Delaware Expediting Services-3",
        options: [
          {
            optionTitle: "Delaware 1hr rush filling",
            description:
              "For the speediest business formation, BizFilings offers a Rush Filing Service option to gain state approval of your incorporation in 1 hour.",
            price: "199",
            included: true,
          },
          {
            optionTitle: "Delaware 1hr rush filling",
            description:
              "For the speediest business formation, BizFilings offers a Rush Filing Service option to gain state approval of your incorporation in 1 hour.",
            price: "199",
            included: true,
          },
          {
            optionTitle: "Delaware 1hr rush filling",
            description:
              "For the speediest business formation, BizFilings offers a Rush Filing Service option to gain state approval of your incorporation in 1 hour.",
            price: "199",
            included: true,
          },
        ],
        packagePrice: 400,
        adonPrice: 300,
        stateFee: 30,
        totalPrice: 730,
      },
    ];
  },
};

var _excel_upload_dialog;
function processExcelFile(cb) {
  //Reference the FileUpload element.
  var fileUpload = document.getElementById("excel_upload_file");
  //Validate whether File is valid Excel file.
  var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
  if (regex.test(fileUpload.value.toLowerCase())) {
    if (typeof FileReader != "undefined") {
      var reader = new FileReader();

      //For Browsers other than IE.
      if (reader.readAsBinaryString) {
        reader.onload = function (e) {
          readExcelData(e.target.result, cb);
        };
        reader.readAsBinaryString(fileUpload.files[0]);
      } else {
        //For IE Browser.
        reader.onload = function (e) {
          var data = "";
          var bytes = new Uint8Array(e.target.result);
          for (var i = 0; i < bytes.byteLength; i++) {
            data += String.fromCharCode(bytes[i]);
          }
          readExcelData(data, cb);
        };
        reader.readAsArrayBuffer(fileUpload.files[0]);
      }
    } else {
      Util.showError("This browser does not support HTML5", "Excel Upload");
    }
  } else {
    Util.showError("Please upload a valid excel file", "Excel Upload");
  }
}
function readExcelData(data, cb) {
  //Read the Excel File data.
  var workbook = XLSX.read(data, {
    type: "binary",
  });

  //old code

  // //Fetch the name of First Sheet.
  // var firstSheet = workbook.SheetNames[0];

  // //Read all rows from First Sheet into an JSON array.
  // var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
  // _excel_upload_dialog.modal('hide');
  // cb(excelRows);

  //new code
  var excelRowsArray = [];
  $.each(workbook.SheetNames, (i, v) => {
    Util.showInfo(`Reading data from: ${v}`, "Excel Data");
    var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[v]);
    excelRowsArray.push({ sheetName: v, data: excelRows });
  });

  _excel_upload_dialog.modal("hide");
  cb(excelRowsArray);
}
BootBoxComponents = {
  ExcelUpload: function (cb) {
    var content = `
		< form enctype = "multipart/form-data" >
			<input id="excel_upload_file" type=file  name="files[]">
		</>
		<button id="saveBtn" type="button" class="btn btn-primary mt-1">Save</button>
		`;
    _excel_upload_dialog = bootbox.dialog({
      title: "Excel Upload",
      message: content,
      size: "large",
      buttons: {
        ok: {
          label: "Close",
          className: "btn-danger",
        },
      },
    });
    $(_excel_upload_dialog)
      .find("#saveBtn")
      .bind("click", () => {
        processExcelFile(cb);
      });
  },
};

setTimeout(() => {
  workingScope.refreshData();
  $(".ws-f").each((i, el) => {
    $(el).val(workingScope.faculty.id);
  });
  $(".ws-y").each((i, el) => {
    $(el).val(workingScope.year.id);
  });
  $(".ws-p").each((i, el) => {
    $(el).val(workingScope.program.id);
    $(el).trigger("change");
  });
}, 2000);
