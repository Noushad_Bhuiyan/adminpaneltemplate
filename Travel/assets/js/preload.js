var roleAndPhotoDiv = $("#roleAndPhotoDiv"),
  defaultImageCoverLink =
    "https://via.placeholder.com/2000x1333.jpg?text=No+Image+Available",
  avatarPreviewDiv = $("#avatarPreviewDiv");
function showDialog(item, dialogMode) {
  _item = item;
  if (dialogMode == "D") {
    showDlgForDelete();
  } else {
    showDlgForSave(dialogMode);
  }
}

function addItem() {
  if (_moduleTitle == "User") {
    showDialog(Object.assign(createModel, { _id: 0 }), "I");
  } else {
    showDialog(Object.assign(model, { _id: 0 }), "I");
  }
}

function validToSave() {
  var valid = Validator.ValidateIn("dvEdit");
  return valid;
}

function deleteItem() {
  Util.deleteData(deleteURL + "/" + _item._id, (res) => {
    Util.showSuccess(
      _moduleTitle + " Deleted Successfully",
      "Delete " + _moduleTitle
    );
    _item = generateTable();
    $(dlg).modal("hide");
  });
}

function beautifyPayload() {
  if (_item.hasOwnProperty("role")) {
    _item.role = _item.role?.toLowerCase();
  } // because role is being stored in lower case in the database

  console.log("Model inside beautify: ");
  console.log(model);

  if (_moduleTitle === "User" && _item._id === 0) {
    Object.fromEntries(
      Object.entries(_item).filter(([key]) => {
        return createModel.hasOwnProperty(key);
      })
    );
  } else {
    Object.fromEntries(
      Object.entries(_item).filter(([key]) => {
        return model.hasOwnProperty(key);
      })
    );
  }
  // Object.entries(_item).forEach(([key, value]) => {
  //   if (!value || value === null || value === undefined) {
  //     delete _item[key];
  //   }
  // });
  console.log("After beautify: ");
  console.log(_item);
}

//TODO:saveItem
function saveItem(e) {
  //var serialized = serializeToJSON("frm");
  console.log("before beautify, inside saveItem: ");
  console.log(_item);

  adaptData($(dlg), false);
  if (!validToSave()) {
    return;
  }

  beautifyPayload();
  console.log("after beautify, inside saveItem: ");
  console.log(_item);
  // console.log(serialized);
  // if (serialized._id != 0) {
  //   Util.updateData(
  //     updateURL + "/" + serialized._id,
  //     serialized,
  //     saveItemCallback
  //   );
  // } else {
  //   if (serialized.hasOwnProperty("_id")) {
  //     delete serialized._id;
  //   }
  //   console.log(serialized);
  //   Util.createData(saveURL, serialized, saveItemCallback);
  // }

  if (_item._id != 0) {
    console.log("inside saveItem, before update: ");
    console.log(_item);
    Util.updateData(updateURL + "/" + _item._id, _item, saveItemCallback);
  } else {
    console.log("inside saveItem, before create: ");
    console.log(_item);
    Util.createData(saveURL, _item, saveItemCallback);
  }
}
function saveItemCallback(res) {
  Util.showSuccess(
    _moduleTitle + " Saved Successfully",
    "Save " + _moduleTitle
  );
  _item = generateTable();
  $(dlg).modal("hide");
}

//TODO:adaptData
function adaptData(dlg, isToDlg) {
  if (!checkAllDDL()) {
    if (populateAllDdl) {
      populateAllDdl();
    }
  }
  if (isToDlg == true) {
    var hasPhoto = false;
    Object.assign(_item, resolveServerData());

    console.log(_item);
    Object.keys(model).forEach((i) => {
      if (i == "photo") {
        hasPhoto = true;
      } else {
        $(dlg)
          .find("#" + i)
          .val(typeof _item[i] === "boolean" ? _item[i].toString() : _item[i]); // toString is to convert the number/boolean to string
        if (
          $(dlg)
            .find("#" + "ddl" + toTitle(i))
            .is("select")
        ) {
          // checking if any data is a dropdown
          changeSelectOption(
            $(dlg).find("#" + "ddl" + toTitle(i)),
            typeof _item[i] === "boolean" ? _item[i].toString() : _item[i]
          );
        }
        if ($(dlg).find("#" + "dateInp" + toTitle(i)).length > 0) {
          // checking if any data is a date input
          $(dlg)
            .find("#" + "dateInp" + toTitle(i))
            .datepicker("getDates")
            .forEach((date, index) => {
              $(dlg)
                .find("#" + "dateInp" + toTitle(i))
                .datepicker("setDate", date);
            });
        }
      }
    });

    console.log("after adaptData: ");
    console.log(_item);
    if (hasPhoto) {
      $("#previewImage").attr("src", _imageUrl + _item.photo);
      if (_item.photo) $("#photoLabel").text(_imageUrl + _item.photo);
    }
  } else {
    console.log("add: ");
    var modelCopy =
      _moduleTitle === "User"
        ? Object.assign(model, createModel)
        : Object.assign({}, model);

    Object.keys(modelCopy).forEach((i) => {
      if (
        $(dlg)
          .find("#" + i)
          .val()
      ) {
        _item[i] = $(dlg)
          .find("#" + i)
          .val();
      }
      if (
        $(dlg)
          .find("#" + "ddl" + toTitle(i))
          .val()
      ) {
        _item[i] = $(dlg)
          .find("#" + "ddl" + toTitle(i))
          .val();
      } // checking if any data is a dropdown
      if ($(dlg).find("#" + "dateInp" + toTitle(i)).length > 0) {
        // checking if any data is a date input
        $(dlg)
          .find("#" + "dateInp" + toTitle(i))
          .datepicker("getDates")
          .forEach((date, index) => {
            $(dlg)
              .find("#" + "dateInp" + toTitle(i))
              .datepicker("getDate", date);
          });
      }
    });
  }
}

function getFilteredDataList(url, expectedModel) {
  var result = [];
  getAllData(url, (res) => {
    res.data.data.forEach((element, index) => {
      result.push(
        Object.fromEntries(
          Object.entries(element).filter(([key]) =>
            expectedModel.hasOwnProperty(key)
          )
        )
      );
    });
  });
  return result;
}

function showDlgForDelete() {
  dlg = $("#dvDelete");
  $(dlg).find("#title").text("Delete");
  $(dlg).find("#message").text("Are you sure to delete?");

  Object.assign(_item, resolveServerData());

  console.log(_item);

  if (deleteDescription) {
    var detailsText = `<br>`,
      count = 0;
    Object.entries(model).forEach((i) => {
      if (count <= 5) {
        if (
          i[0] != "photo" &&
          i[0] != "_id" &&
          i[1] != undefined &&
          typeof i[1] != "object"
        ) {
          if (fitModel.hasOwnProperty(i[0])) {
            detailsText += `${i[0]}</b>: ${
              window[i[0] + "sData"].find((o) => o._id === _item[i[0]])?.name ||
              "Unknown"
            }<br>`;
          } else {
            detailsText += `${i[0]}: ${_item[i[0]]}<br>`;
          }
        }
        count++;
      }
    });
    $(dlg).find("#details").html(detailsText);
  } else {
    $(dlg).find("#details").html("");
  }
  $(dlg).modal("show");
}

function setupUserAddModal(dlg) {
  if ($(dlg).find("#password").length == 0) {
    $(`<div class="row" id="passwordFields">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <label for="password" class="col-form-label required">Password</label>
                <div class="form-group">
                    <input class="form-control p-1" type="password" name="password" id="password"
                    placeholder="Password" required="" spellcheck="false" data-ms-editor="true">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <label class="col-form-label" for="passwordConfirm">Confirm Password</label>
                <div class="custom-file">
                   <input class="form-control p-1" type="password" name="passwordConfirm" id="passwordConfirm"
                   placeholder="Confirm Password" required="" spellcheck="false" data-ms-editor="true">
                </div>
            </div>

      </div>`).appendTo(dlg.find("#frm .modal-body"));
  }
  if ($(dlg).find("#avatarPreviewDiv").length > 0) {
    avatarPreviewDiv.remove();
  }
  if ($(dlg).find("#roleAndPhotoDiv").length > 0) {
    roleAndPhotoDiv.remove();
  }
}
function setupUserEditModal(dlg) {
  if (
    $(dlg).find("#password").length !== 0 &&
    $(dlg).find("#passwordConfirm").length !== 0
  ) {
    $(`#passwordFields`).remove();
  }
  if ($(dlg).find("#avatarPreviewDiv").length == 0) {
    avatarPreviewDiv.prependTo(dlg.find("#frm .modal-body #nameAndEmailDiv"));
  }
  if ($(dlg).find("#roleAndPhotoDiv").length == 0) {
    roleAndPhotoDiv.appendTo(dlg.find("#frm .modal-body"));
  }
}

function showDlgForSave(dialogMode) {
  dlg = $("#dvEdit");
  $(dlg).find("#frm").trigger("reset");
  $(dlg)
    .find(".add-edit")
    .html(dialogMode == "I" ? "New " + _moduleTitle : "Edit " + _moduleTitle);
  if (_moduleTitle == "User") {
    if (dialogMode == "I") {
      setupUserAddModal(dlg);
    } else {
      setupUserEditModal(dlg);
    }
  }
  adaptData(dlg, true);
  $(dlg).modal("show");
}

// function to convert a string to title case
function toTitle(str) {
  return str.replace(/(^|\s)\S/g, function (t) {
    return t.toUpperCase();
  });
}

//function to convert serialized data to JSON format
function serializeToJSON(formId) {
  var formdata = $("#" + formId).serializeArray();
  var data = {};
  $(formdata).each(function (index, obj) {
    data[obj.name] = obj.value;
  });
  return data;
}

//function to check if all ddl are not populated
function checkAllDDL() {
  var isValid = 0;
  Object.values($('[id^="ddl"]')).forEach((el) => {
    if ($(el).has("option").length > 1) {
      isValid++;
    }
  });
  if (isValid == $('[id^="ddl"]').length + 1) {
    console.log("all dropdowns are populated");
    return true;
  }
  return false;
}

//function to resolve the server data and return the data in the format of the model
function resolveServerData() {
  var result = {};
  if (!isResolved) {
    if (fitModel) {
      Object.entries(fitModel).forEach((el) => {
        result[el[0]] = getProperty(el[1], _item);
      });
      isResolved = true;
    }
  }
  return result;
}

//function to get thefile name from the file path
function getFileName() {
  return window.location.href.replace(/^.*[\\\/]/, "").split("s.")[0];
}

// function to access nested data by string
function getProperty(path, obj = self, separator = ".") {
  var properties = Array.isArray(path) ? path : path.split(separator);
  return properties.reduce((prev, curr) => prev && prev[curr], obj);
}

// function to set nested data by string
setNestedProp = (obj = {}, [first, ...rest], value) => ({
  ...obj,
  [first]: rest.length ? setNestedProp(obj[first], rest, value) : value,
});

//function to restore the flattened nested data
function restoreNestedData(data) {}

//function to change the select option value
function changeSelectOption(ddl, value) {
  $(ddl).val(value);
  $(ddl).trigger("change");
}

//function to display rating stars according a integer values
function displayRating(rating) {
  //make the rating integer
  var result = "";
  result += `<p class="form-label text-center">${rating}</p>`;
  rating = Math.ceil(rating);

  for (var i = 0; i < rating; i++) {
    result += `<span class="ni ni-star-fill checked text-primary"></span>`;
  }
  for (var i = rating; i < 5; i++) {
    result += `<span class="ni ni-star-fill"></span>`;
  }
  return result;
}

// function to transform complex JSON into formdata
function transformInToFormObject(data) {
  let formData = new FormData();
  for (let key in data) {
    if (Array.isArray(data[key])) {
      data[key].forEach((obj, index) => {
        let keyList = Object.keys(obj);
        keyList.forEach((keyItem) => {
          let keyName = [key, "[", index, "]", ".", keyItem].join("");
          formData.append(keyName, obj[keyItem]);
        });
      });
    } else if (typeof data[key] === "object") {
      for (let innerKey in data[key]) {
        formData.append(`${key}.${innerKey}`, data[key][innerKey]);
      }
    } else {
      formData.append(key, data[key]);
    }
  }
  return formData;
}
