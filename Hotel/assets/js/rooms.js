const addButton = document.getElementById("modalTableAddBtn");

function modalRowHandler(item, dialogMode) {
  _item = item;
  if (dialogMode == "D") {
    modalRowDelete();
  } else {
    modalRowSave(dialogMode);
  }
}

function modalRowDelete() {
  dlg = $("#modalRowDelete");
  $(dlg).find("#title").text("Delete");
  $(dlg).find("#message").text("Are you sure to delete?");
  console.log(_item);
  $(dlg).find("#details").html("");
  $(dlg).modal("show");
}

function modalRowSave(dialogMode) {
  dlg = $("#modalRowEdit");
  //$(dlg).find("#frm").trigger("reset");
  $(dlg)
    .find(".add-edit")
    .html(dialogMode == "I" ? "New Amenity" : "Edit Amenity");
  $(dlg).modal("show");
}
